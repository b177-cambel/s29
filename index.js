// Use the "require" directive to load the express module/package
const express = require("express");

// Create an application using express
const app = express();

// Port to listen to
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows your app to read json data
app.use(express.json());

// Allows the app to read data from forms
// Applying the option "extended:true" allows us to recieve information in other data types such as an object
app.use(express.urlencoded({extended:true}));

// Create a GET route
app.get("/", (req, res) => {
	res.send("Hello, world!");
})

// Another GET route with "/hello" endpoint
app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
})

// Create a POST route
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

let users = [];

// Create a POST route to register a user
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
	}
	else{
		res.send("Please input BOTH username and password.");
	}
});

// Create a PUT route to change the password of a specific user.
app.put("/change-password",(req, res) => {
	// Create a variable to store the message to be sent back to the client
	let message;

	// Create a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length;i++){

		// req.body == juan23 -- users[i].username == jane123
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;

			// Changes the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated.`

			break;
		}
		else{
			message = "User does not exist";
		}
	}
	res.send(message);
})

app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
})

app.get("/users", (req, res) => {
	res.send(users);
})

app.delete("/delete-user", (req, res) => {
	res.send(`User ${req.body.username} has been deleted.`);
})



// Tells our server to listend to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
